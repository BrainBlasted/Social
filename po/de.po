# German translation for social.
# Copyright (C) 2020 social's COPYRIGHT HOLDER
# This file is distributed under the same license as the social package.
# Philipp Kiemle <philipp.kiemle@gmail.com>, 2020.
# Tim Sabsch <tim@sabsch.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: social main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Social/issues\n"
"POT-Creation-Date: 2020-11-25 18:39+0000\n"
"PO-Revision-Date: 2021-01-30 22:22+0100\n"
"Last-Translator: Tim Sabsch <tim@sabsch.com>\n"
"Language-Team: German <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 3.38.0\n"

#: data/gtk/account_popover.ui:12
msgid "Account Settings"
msgstr "Kontoeinstellungen"

#: data/gtk/account_popover.ui:16
msgid "Blocked Users"
msgstr "Blockierte Nutzer"

#: data/gtk/account_popover.ui:22
msgid "Log Out"
msgstr "Abmelden"

#: data/gtk/help-overlay.ui:10
msgctxt "shortcut window"
msgid "General Shortcuts"
msgstr "Allgemeine Tastenkürzel"

#: data/gtk/help-overlay.ui:13
msgctxt "shortcut window"
msgid "Create New Post"
msgstr "Neuen Beitrag erstellen"

#: data/gtk/help-overlay.ui:19
msgctxt "shortcut window"
msgid "Show Primary Menu"
msgstr "Primäres Menü anzeigen"

#: data/gtk/help-overlay.ui:25
msgctxt "shortcut window"
msgid "Back"
msgstr "Zurück"

#: data/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "Tastenkürzel anzeigen"

#: data/gtk/help-overlay.ui:37
msgctxt "shortcut window"
msgid "Quit"
msgstr "Beenden"

#: data/gtk/main_content_page.ui:27 data/org.gnome.Social.appdata.xml.in.in:4
#: data/org.gnome.Social.desktop.in.in:3 src/window.rs:51
msgid "Social"
msgstr "Social"

#: data/gtk/main_content_page.ui:51
msgid "Home"
msgstr "Start"

#: data/gtk/main_content_page.ui:63
msgid "Community"
msgstr "Community"

#: data/gtk/main_content_page.ui:75
msgid "World"
msgstr "Welt"

#: data/gtk/post_builder.ui:14
msgid "New Post"
msgstr "Neuer Beitrag"

#: data/gtk/post_builder.ui:25
msgid "_Cancel"
msgstr "A_bbrechen"

#: data/gtk/post_builder.ui:33
msgid "_Post"
msgstr "_Veröffentlichen"

#: data/gtk/primary_menu.ui:7
msgid "_Search"
msgstr "_Suchen"

#: data/gtk/primary_menu.ui:11
msgid "_Favorites"
msgstr "_Favoriten"

#: data/gtk/primary_menu.ui:15
msgid "_Lists"
msgstr "_Listen"

#: data/gtk/primary_menu.ui:21
msgid "_Keyboard Shortcuts"
msgstr "_Tastenkürzel"

#: data/gtk/primary_menu.ui:25
msgid "_About Social"
msgstr "_Info zu Social"

#: data/gtk/register_page.ui:9
msgid "Register"
msgstr "Registrieren"

#: data/gtk/register_page.ui:32
msgid "Instance"
msgstr "Instanz"

#: data/gtk/register_page.ui:50
msgid "Authorize"
msgstr "Legitimieren"

#: data/gtk/toot_widget.ui:73
msgid "This toot could not properly be displayed"
msgstr "Dieser toot konnte nicht richtig angezeigt werden"

#: data/org.gnome.Social.appdata.xml.in.in:7
msgid "Christopher Davis"
msgstr "Christopher Davis"

#: data/org.gnome.Social.appdata.xml.in.in:8
msgid "Federated microblogging client"
msgstr "Dezentrale Mikroblogging-Anwendung"

#: data/org.gnome.Social.appdata.xml.in.in:11
msgid "Social is a federated microblogging client for GNOME written in Rust."
msgstr ""
"Social ist eine dezentrale Mikroblogging-Anwendung für GNOME, geschrieben in "
"Rust."

#: data/org.gnome.Social.desktop.in.in:4
msgid "Social is a federated microblogging client for the GNOME desktop"
msgstr ""
"Social ist eine dezentrale Mikroblogging-Anwendung für die GNOME "
"Arbeitsumgebung"

#: data/org.gnome.Social.desktop.in.in:5
msgid "Social microblogging"
msgstr "Soziales Microblogging"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Social.desktop.in.in:17
msgid "Mastodon;joinmastodon.org;PixelFed;pixelfed.org;mastodon.social;"
msgstr "Mastodon;joinmastodon.org;PixelFed;pixelfed.org;mastodon.social;"

#: src/app.rs:255
msgid "A federated microblogging client for GNOME"
msgstr "Eine dezentrale Mikroblogging-Anwendung für GNOME"

#: src/app.rs:256
msgid "translator-credits"
msgstr "Philipp Kiemle <philipp.kiemle@gmail.com>, 2020."

#: src/widgets/notification_widget.rs:48
msgid "<b>{}</b> followed you"
msgstr "<b>{}</b> folgt Ihnen jetzt"

#: src/widgets/notification_widget.rs:51
msgid "<b>{}</b> favorited your post"
msgstr "<b>{}</b> favorisierte Ihren Beitrag"

#: src/widgets/notification_widget.rs:55
msgid "<b>{}</b> reblogged your post"
msgstr "<b>{}</b> rebloggte Ihren Beitrag"

#: src/widgets/notification_widget.rs:59
msgid "<b>{}</b> mentioned you"
msgstr "<b>{}</b> hat Sie erwähnt"

#: src/widgets/pages/notifications_page.rs:92
msgid "Notifications"
msgstr "Benachrichtigungen"

#: src/widgets/pages/register_page.rs:134
msgid "Failed to authorize URL"
msgstr "URL konnte nicht legitimiert werden"

#: src/widgets/post_builder.rs:65
msgid "Reply"
msgstr "Antworten"

#: src/widgets/post_builder.rs:66
msgid "_Reply"
msgstr "_Antworten"

#: src/widgets/post_builder.rs:85 src/widgets/post_builder.rs:100
msgid "{} character remaining"
msgid_plural "{} characters remaining"
msgstr[0] "{} Zeichen verbleibend"
msgstr[1] "{} Zeichen verbleibend"

#: src/widgets/post_builder.rs:188
msgid "Failed to post status"
msgstr "Status konnte nicht veröffentlicht werden"

#: src/widgets/toot_widget.rs:76
msgid "Show Less"
msgstr "Weniger anzeigen"

#: src/widgets/toot_widget.rs:78 src/widgets/toot_widget.rs:338
msgid "Show More…"
msgstr "Mehr anzeigen …"
