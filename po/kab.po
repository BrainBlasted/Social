# Kabyle translation for social.
# Copyright (C) 2024 social's COPYRIGHT HOLDER
# This file is distributed under the same license as the social package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# ButterflyOfFire <ButterflyOfFire@protonmail.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: social main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Social/issues\n"
"POT-Creation-Date: 2023-10-21 09:04+0000\n"
"PO-Revision-Date: 2024-03-09 11:04+0100\n"
"Last-Translator: ButterflyOfFire <ButterflyOfFire@protonmail.com>\n"
"Language-Team: French <ButterflyOfFire@protonmail.com>\n"
"Language: kab\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-DL-Team: kab\n"
"X-DL-Module: social\n"
"X-DL-Branch: main\n"
"X-DL-Domain: po\n"
"X-DL-State: None\n"
"X-Generator: Gtranslator 41.0\n"

#: data/gtk/account_popover.ui:12
msgid "Account Settings"
msgstr "Iɣewwarṛen n umiḍan"

#: data/gtk/account_popover.ui:16
msgid "Blocked Users"
msgstr ""

#: data/gtk/account_popover.ui:22
msgid "Log Out"
msgstr "Ffeɣ"

#: data/gtk/help-overlay.ui:10
msgctxt "shortcut window"
msgid "General Shortcuts"
msgstr ""

#: data/gtk/help-overlay.ui:13
msgctxt "shortcut window"
msgid "Create New Post"
msgstr "Snulfu-d tasuffeɣt tamaynut"

#: data/gtk/help-overlay.ui:19
msgctxt "shortcut window"
msgid "Show Primary Menu"
msgstr ""

#: data/gtk/help-overlay.ui:25
msgctxt "shortcut window"
msgid "Back"
msgstr "Uɣal"

#: data/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "Sken-d inegzumen n unasiw"

#: data/gtk/help-overlay.ui:37
msgctxt "shortcut window"
msgid "Quit"
msgstr "Ffeɣ"

#: data/gtk/main_content_page.ui:27 data/org.gnome.Social.appdata.xml.in.in:4
#: data/org.gnome.Social.desktop.in.in:3 src/window.rs:51
msgid "Social"
msgstr ""

#: data/gtk/main_content_page.ui:51
msgid "Home"
msgstr "Agejdan"

#: data/gtk/main_content_page.ui:63
msgid "Community"
msgstr ""

#: data/gtk/main_content_page.ui:75
msgid "World"
msgstr "Amaḍal"

#: data/gtk/post_builder.ui:14
msgid "New Post"
msgstr "Tasuffeɣt tamaynut"

#: data/gtk/post_builder.ui:25
msgid "_Cancel"
msgstr "Sefsex (_C)"

#: data/gtk/post_builder.ui:33
msgid "_Post"
msgstr "Suffeɣ (_P)"

#: data/gtk/primary_menu.ui:7
msgid "_Search"
msgstr "Anadi (_S)"

#: data/gtk/primary_menu.ui:11
msgid "_Favorites"
msgstr "Imenya_fen"

#: data/gtk/primary_menu.ui:15
msgid "_Lists"
msgstr "Tibdarin (L)"

#: data/gtk/primary_menu.ui:21
msgid "_Keyboard Shortcuts"
msgstr "Inegzumen n unasiw (_K)"

#: data/gtk/primary_menu.ui:25
msgid "_About Social"
msgstr ""

#: data/gtk/register_page.ui:9
msgid "Register"
msgstr "Snulfu-d ami"

#: data/gtk/register_page.ui:32
msgid "Instance"
msgstr "Aqeddac"

#: data/gtk/register_page.ui:50
msgid "Authorize"
msgstr "Sireg"

#: data/gtk/toot_widget.ui:73
msgid "This toot could not properly be displayed"
msgstr ""

#: data/org.gnome.Social.appdata.xml.in.in:7
msgid "Christopher Davis"
msgstr "Christopher Davis"

#: data/org.gnome.Social.appdata.xml.in.in:8
msgid "Federated microblogging client"
msgstr ""

#: data/org.gnome.Social.appdata.xml.in.in:11
msgid "Social is a federated microblogging client for GNOME written in Rust."
msgstr ""

#: data/org.gnome.Social.desktop.in.in:4
msgid "Social is a federated microblogging client for the GNOME desktop"
msgstr ""

#: data/org.gnome.Social.desktop.in.in:5
msgid "Social microblogging"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Social.desktop.in.in:17
msgid "Mastodon;joinmastodon.org;PixelFed;pixelfed.org;mastodon.social;"
msgstr "Mastodon;joinmastodon.org;PixelFed;pixelfed.org;mastodon.social;"

#: src/app.rs:255
msgid "A federated microblogging client for GNOME"
msgstr ""

#: src/app.rs:256
msgid "translator-credits"
msgstr ""

#: src/widgets/notification_widget.rs:48
msgid "<b>{}</b> followed you"
msgstr ""

#: src/widgets/notification_widget.rs:51
msgid "<b>{}</b> favorited your post"
msgstr ""

#: src/widgets/notification_widget.rs:55
msgid "<b>{}</b> reblogged your post"
msgstr ""

#: src/widgets/notification_widget.rs:59
msgid "<b>{}</b> mentioned you"
msgstr ""

#: src/widgets/pages/notifications_page.rs:92
msgid "Notifications"
msgstr "Ilɣa"

#: src/widgets/pages/register_page.rs:134
msgid "Failed to authorize URL"
msgstr ""

#: src/widgets/post_builder.rs:65
msgid "Reply"
msgstr "Err"

#: src/widgets/post_builder.rs:66
msgid "_Reply"
msgstr "E_rr"

#: src/widgets/post_builder.rs:85 src/widgets/post_builder.rs:100
msgid "{} character remaining"
msgid_plural "{} characters remaining"
msgstr[0] ""
msgstr[1] ""

#: src/widgets/post_builder.rs:188
msgid "Failed to post status"
msgstr ""

#: src/widgets/toot_widget.rs:76
msgid "Show Less"
msgstr "Sken-d drus"

#: src/widgets/toot_widget.rs:78 src/widgets/toot_widget.rs:338
msgid "Show More…"
msgstr "Sken-d ugar…"
