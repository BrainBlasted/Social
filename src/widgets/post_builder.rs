// post_builder.rs
//
// Copyright 2019 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;

use elefren::entities::status::Status;
use elefren::{MastodonClient, StatusBuilder};

use std::cell::RefCell;
use std::convert::TryInto;
use std::rc::Rc;
use std::sync::Arc;
use std::sync::RwLock;

use crate::ClientRef;
use crate::SoclApplication;

use crate::i18n::*;

pub struct PostBuilder {
    widget: gtk::Dialog,
    title: gtk::Label,
    post: gtk::Button,
    post_input: gtk::TextView,
    buffer: gtk::TextBuffer,
    char_count_lbl: gtk::Label,
    client: ClientRef,
    reply_to: Option<String>,
    reply_to_full: RefCell<Option<Status>>,
}

impl PostBuilder {
    pub fn new(
        window: &libhandy::ApplicationWindow,
        client: ClientRef,
        reply_to: Option<String>,
    ) -> Rc<Self> {
        let mut post_builder = Self::default();
        post_builder.reply_to = reply_to;
        post_builder.client = client;

        let client = post_builder.client.read().unwrap();
        let mut char_count: i32 = 500;
        if let Ok(instance) = client.as_ref().unwrap().instance() {
            char_count = instance.max_toot_chars.unwrap_or(500).try_into().unwrap();
        };

        if let Some(ref id) = post_builder.reply_to {
            post_builder.title.set_label(&i18n("Reply"));
            post_builder.post.set_label(&i18n("_Reply"));
            let status = client.as_ref().unwrap().get_status(&id);

            let _ = status.map(|s| {
                post_builder.reply_to_full.replace(Some(s.clone()));
                let mut iter = post_builder.buffer.get_start_iter();
                let account_str = format!("@{} ", s.account.acct);
                post_builder.buffer.insert(&mut iter, &account_str);
                for m in s.mentions.iter() {
                    let account_str = format!("@{} ", m.acct);
                    post_builder.buffer.insert(&mut iter, &account_str);
                }
            });
        }

        // Drop the borrow
        drop(client);

        let chars_text = ni18n_f(
            "{} character remaining",
            "{} characters remaining",
            char_count as u32,
            &[&char_count.to_string()],
        );
        post_builder.char_count_lbl.set_label(&chars_text);

        post_builder.widget.set_transient_for(Some(window));
        post_builder.widget.set_modal(true);

        post_builder.buffer.connect_changed(clone!(@weak post_builder.post as post, @weak post_builder.char_count_lbl as lbl => move |buffer| {
            let chars_used = buffer.get_char_count();
            post.set_sensitive(chars_used < char_count);
            let diff = char_count - chars_used;
            let txt = ni18n_f(
                "{} character remaining",
                "{} characters remaining",
                diff.abs() as u32,
                &[&diff.to_string()],
            );
            lbl.set_label(&txt);
        }));

        actions::create(&post_builder);

        Rc::new(post_builder)
    }

    pub fn present(&self) {
        self.post_input.grab_focus();
        self.widget.present();
    }
}

impl Default for PostBuilder {
    fn default() -> Self {
        let builder = gtk::Builder::from_resource("/org/gnome/Social/gtk/post_builder.ui");

        let widget = builder.get_object::<gtk::Dialog>("post_builder").unwrap();
        widget.set_application(Some(&SoclApplication::get_default().unwrap()));

        let post = builder.get_object("post_button").unwrap();
        let title = builder.get_object("title").unwrap();
        let post_input = builder.get_object::<gtk::TextView>("post_input").unwrap();
        let char_count_lbl = builder.get_object("characters_left_label").unwrap();

        let buffer = post_input.get_buffer().unwrap().downcast().unwrap();

        Self {
            widget,
            post,
            title,
            post_input,
            buffer,
            char_count_lbl,
            client: Arc::new(RwLock::new(None)),
            reply_to: None,
            reply_to_full: RefCell::new(None),
        }
    }
}

mod actions {
    use super::*;
    use crate::utils;
    use gio::ActionMapExt;

    pub fn create(post_builder: &PostBuilder) {
        let group = gio::SimpleActionGroup::new();
        action!(
            group,
            "cancel",
            clone!(@weak post_builder.widget as dialog => move |_, _| {
                dialog.close();
            })
        );

        let reply_to = post_builder.reply_to.clone();
        let reply_to_full = post_builder.reply_to_full.clone();
        action!(
            group,
            "post",
            clone!(@weak post_builder.widget as dialog, @weak post_builder.buffer as buffer, @weak post_builder.client as client => move |_, _| {
                dialog.hide();
                if let Some(gstr) =
                    buffer.get_text(&buffer.get_start_iter(), &buffer.get_end_iter(), false)
                {
                    let mut status_builder = StatusBuilder::new();
                    status_builder.status(gstr.to_string());
                    reply_to.as_ref().map(|id| status_builder.in_reply_to(id));
                    if let Some(stat) = reply_to_full.borrow().as_ref() {
                        if !stat.spoiler_text.is_empty() {
                            status_builder.spoiler_text(&stat.spoiler_text);
                        }
                        status_builder.visibility(stat.visibility);
                    };

                    let status = status_builder.build().unwrap();

                    match client.read().unwrap().as_ref().unwrap().new_status(status) {
                        Ok(_) => dialog.close(),
                        Err(_) => {
                            dialog.show();
                            utils::error_dialog(&i18n("Failed to post status"), &dialog)
                        }
                    }
                }
            })
        );

        post_builder
            .widget
            .insert_action_group("post-builder", Some(&group));
        let application = post_builder.widget.get_application().unwrap();
        application.set_accels_for_action("post-builder.post", &["<Primary>p"]);
    }
}
