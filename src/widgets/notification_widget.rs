// notification_widget.rs
//
// Copyright 2019 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;

use elefren::entities::notification::Notification;
use elefren::entities::notification::NotificationType;

use std::rc::Rc;
use std::sync::Arc;
use std::sync::RwLock;

use crate::i18n::*;
use crate::widgets::TootWidget;
use crate::ClientRef;

#[derive(Clone)]
pub struct NotificationWidget {
    pub container: gtk::Box,
    label: gtk::Label,
    status_widget: Option<Rc<TootWidget>>,
    client: ClientRef,
}

impl NotificationWidget {
    pub fn new(client: ClientRef, notif: Notification) -> Rc<Self> {
        let mut widget = Self::default();
        widget.client = client;

        let markup = match notif.notification_type {
            NotificationType::Follow => {
                i18n_f("<b>{}</b> followed you", &[&notif.account.display_name])
            }
            NotificationType::Favourite => i18n_f(
                "<b>{}</b> favorited your post",
                &[&notif.account.display_name],
            ),
            NotificationType::Reblog => i18n_f(
                "<b>{}</b> reblogged your post",
                &[&notif.account.display_name],
            ),
            NotificationType::Mention => {
                i18n_f("<b>{}</b> mentioned you", &[&notif.account.display_name])
            }
        };

        widget.label.set_markup(&markup);

        Rc::new(widget)
    }
}

impl Default for NotificationWidget {
    fn default() -> Self {
        let container = gtk::Box::new(gtk::Orientation::Vertical, 6);
        container.set_margin_start(6);
        container.set_margin_end(6);
        container.set_margin_top(6);
        container.set_margin_bottom(6);

        let label = gtk::Label::new(None);
        label.set_halign(gtk::Align::Start);
        label.set_xalign(0.0);
        label.set_wrap(true);
        label.set_wrap_mode(pango::WrapMode::WordChar);

        container.append(&label);

        let status_widget = None;
        let client = Arc::new(RwLock::new(None));

        Self {
            container,
            label,
            status_widget,
            client,
        }
    }
}
