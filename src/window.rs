// window.rs
//
// Copyright 2019 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gio::prelude::*;
use gtk::prelude::*;

use elefren::Mastodon;

use std::sync::{Arc, RwLock};

use crossbeam::channel::{unbounded, Receiver, Sender};

use crate::i18n::*;
use crate::{
    app::{Actions, SoclApplication},
    config, passwords, widgets,
};

pub type ClientRef = Arc<RwLock<Option<Mastodon>>>;

#[derive(Clone)]
pub struct SoclWindow {
    pub app: SoclApplication,
    pub window: libhandy::ApplicationWindow,
    pub main_stack: gtk::Stack,
    pub client: ClientRef,
    pub sender: Sender<Actions>,
    pub receiver: Receiver<Actions>,
}

impl SoclWindow {
    pub fn new(application: &SoclApplication) -> Self {
        let window = libhandy::ApplicationWindow::new(application);

        window.set_title(&i18n("Social"));
        window.set_default_size(800, 600);

        let (sender, receiver) = unbounded();

        let main_stack = gtk::Stack::new();
        let register_page = widgets::RegisterPage::new();

        main_stack.set_transition_type(gtk::StackTransitionType::SlideLeftRight);

        main_stack.add_named(&register_page, "register");
        main_stack.set_visible_child_name("register");

        libhandy::ApplicationWindowExt::set_child(&window, Some(&main_stack));

        if !config::PROFILE.is_empty() {
            window.get_style_context().add_class("devel");
        }

        let app = application.clone();
        let window = Self {
            app,
            window,
            main_stack,
            client: Arc::new(RwLock::new(None)),
            sender,
            receiver,
        };

        window.setup_win_actions();

        if let Some(data) = passwords::get_data() {
            let client = Mastodon::from(data);
            let mut client_writer = window.client.write().unwrap();
            *client_writer = Some(client);
            drop(client_writer);
            window.setup_main_content();
        };

        window
    }

    pub fn setup_main_content(&self) {
        let main_content = widgets::MainContentPage::new(self.client.clone());
        let notifications = widgets::NotificationsPage::new(self.client.clone());

        self.main_stack.add_named(&main_content, "main");
        self.main_stack.add_named(&notifications, "notifications");
        self.main_stack.set_visible_child_name("main");

        main_content.init();
        notifications.init();
    }

    fn update_notification_widgets(
        stack: &gtk::Stack,
        btn: &gtk::Button,
        page: &widgets::NotificationsPage,
    ) {
        if let Some(stack_page) = stack.get_page(page) {
            if stack_page.get_needs_attention() {
                btn.get_style_context().add_class("needs-attention");
            } else {
                btn.get_style_context().remove_class("needs-attention");
            }
        }
    }

    pub fn setup_win_actions(&self) {
        action!(
            self.window,
            "new-post",
            clone!(@weak self.client as client, @weak self.window as win => move |_, _| {
                let post_builder = widgets::PostBuilder::new(&win, client, None);
                post_builder.present();
            })
        );
        self.app
            .set_accels_for_action("win.new-post", &["<Primary>n"]);

        action!(
            self.window,
            "show-primary-menu",
            clone!(@weak self.main_stack as stack => move |_, _| {
                let main_page = stack
                    .get_child_by_name("main")
                    .unwrap()
                    .downcast::<widgets::MainContentPage>()
                    .unwrap();
                main_page.show_menu();
            })
        );
        self.app
            .set_accels_for_action("win.show-primary-menu", &["F10"]);

        action!(
            self.window,
            "notifications",
            clone!(@weak self.main_stack as stack => move |_, _| {
                stack.set_visible_child_name("notifications");
                let main_page = stack
                    .get_child_by_name("main")
                    .unwrap()
                    .downcast::<widgets::MainContentPage>()
                    .unwrap();
                if let Some(child) = stack.get_child_by_name("notifications") {
                    let page = stack.get_page(&child).unwrap();
                    page.set_needs_attention(false);
                    let page = child.downcast_ref::<widgets::NotificationsPage>().unwrap();
                    SoclWindow::update_notification_widgets(
                        &stack,
                        main_page.get_notifs_button(),
                        &page,
                    );
                };
            })
        );

        // TODO: Make a real state machine, prefer HdyDeck for subviews
        action!(
            self.window,
            "back",
            clone!(@weak self.main_stack as stack => move |_, _| {
                if let Some(name) = stack.get_visible_child_name() {
                    if &name.to_string() == "notifications" {
                        stack.set_visible_child_name("main");
                    }
                }
            })
        );
        self.app.set_accels_for_action("win.back", &["Escape"]);
    }

    pub fn present(&self) {
        self.window.present();
    }
}
