# Social

## What is Social?

Social is a new WIP client for [Mastodon](https://mastodon.social/about/more) and [Pleroma](https://pleroma.social/).

## Why Social?

When getting started on the fediverse, I found that the only usable application for Linux was an electron app using an indicator. Given that
GNOME phased out indicators and I personally resent running more than one
electron app at once, I decided to make one myself. Since a Rust API 
wrapper [already existed](https://github.com/pwoolcoc/elefren), I
decided to take the opportunity to learn the language while I worked.

## What can it do?

Right now during the transition to using GTK+, Social can't do much.
Previously Social had the following features:

* Multi-account
* Text-only statuses
* Public timeline viewing
* Viewing instance information

Over the past few months I've been working to make
Social into a minimum viable product. Currently users can:

* Post text-only statuses (no CWs)
* Read text-only statuses
* Reply to statuses
* Favorite and boost statuses
* View a section of the home timeline

## What's next?

Features I am working on now:

* Avatar loading
* Dynamic timelines
* Following
* Custom Emoji support

## Building

### GNOME Builder (Recommended)

GNOME Builder is the environment used for the development of this 
application.It can use Flatpak manifests to create a consistent building
and running environment cross-distro. Thus, it is highly recommended you 
use it.

1. Download [GNOME Builder](https://flathub.org/apps/details/org.gnome.Builder).
2. In Builder, click the "Clone Repository" button at the bottom, using `git@gitlab.gnome.org/World/Social.git`
or `https://gitlab.gnome.org/World/Social.git` as the URL.
3. Click the build button at the top once the project is loaded.

**Note: Social must be installed on the system for the authentication to work. Please refer to the installation section below.**

### Manual

If you feel at home in the terminal or would like to build outside of builder,
these are the commands to use:

```bash
git clone https://gitlab.gnome.org/World/Social.git
cd Social
meson _build --prefix=$PREFIX_DIR  # Defaults to /usr/local
ninja -C _build
```

## Installation

Depending on how you want it installed instructions can differ. If you
used GNOME Builder to build it, clicking the bar at the top window will 
open a submenu with "Export Bundle". This will create a flatpak bundle, 
which can be installed on any system that supports Flatpak.

If you used the commands in the "Manual" section, or want to package for
distribution, you can use the following:

```bash
# Can be changed with the environment variable $DEST_DIR; defaults to prefix
ninja -C _build install 
```


## License information
```
Social: A Mastodon and Pleroma client
Copyright (C) 2017-2019 Christopher Davis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
